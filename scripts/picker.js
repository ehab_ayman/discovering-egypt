/**
 * (c) Facebook, Inc. and its affiliates. Confidential and proprietary.
 */

//==============================================================================
// Welcome to scripting in Spark AR Studio! Helpful links:
//
// Scripting Basics - https://fb.me/spark-scripting-basics
// Reactive Programming - https://fb.me/spark-reactive-programming
// Scripting Object Reference - https://fb.me/spark-scripting-reference
// Changelogs - https://fb.me/spark-changelog
//==============================================================================

// How to load in modules
const Scene = require('Scene');
const Patches = require('Patches');

// Use export keyword to make a symbol available in scripting debug console
export const Diagnostics = require('Diagnostics');

const NativeUI = require('NativeUI');
const Textures = require('Textures');
var index = 0;
var Models = [];
var Texts = [];


(async function () {
    const T1 = await Textures.findFirst('T1');
    const T2 = await Textures.findFirst('T2');
    const T3 = await Textures.findFirst('T3');
    const T4 = await Textures.findFirst('T4');
    const T5 = await Textures.findFirst('T5');

    const M1 = await Scene.root.findFirst('M1');
    const M2 = await Scene.root.findFirst('M2');

    const M3 = await Scene.root.findFirst('M3');

    const M4 = await Scene.root.findFirst('M4');

    const M5 = await Scene.root.findFirst('M5');

    const Text1 = await Scene.root.findFirst('T1');
    const Text2 = await Scene.root.findFirst('T2');
    const Text3 = await Scene.root.findFirst('T3');
    const Text4 = await Scene.root.findFirst('T4');
    const Text5 = await Scene.root.findFirst('T5');

    Models.push(M1, M2, M3, M4, M5)
    Texts.push(Text1,Text2,Text3,Text4,Text5)

    Diagnostics.log(Models.length)
    const picker = NativeUI.picker;

    var correctIndex;
    var correctOldIndex;

    const configuration = {
        selectedIndex: index,
        items: [
            { image_texture: T1 },
            { image_texture: T2 },
            { image_texture: T3 },
            { image_texture: T4 },
            { image_texture: T5 }
        ]
    };
    picker.configure(configuration);
    picker.visible = true;
    Patches.inputs.setScalar('pickerIndex', index + 1);
    picker.selectedIndex.monitor().subscribe(function (index) {
        Diagnostics.log(index.newValue)
     
        Models[index.newValue].hidden = false;
        Models[index.oldValue].hidden = true;
        Texts[index.newValue].hidden = false;
        Texts[index.oldValue].hidden = true;

        //Scene.root.findFirst('M' + correctIndex).hidden = false;
        //Patches.inputs.setScalar('pickerIndex', correctIndex);
    });
})();



